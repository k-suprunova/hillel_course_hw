class Unit:

    _name = ''
    _health = 0
    _attack = 0
    _defence = 0

    def __init__(self, name, health, attack, defence):
        self.name = name
        self.health = health
        self.attack = attack
        self.defence = defence

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        """
        checks if the name that was set is a string
        """
        if isinstance(val, str):
            self._name = val
        else:
            raise ValueError("Name should be a string.")

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, val):
        """
        checks if the health that was set is an integer
        if health is less than 0 sets it as 0
        """
        if isinstance(val, int):
            self._health = val
        else:
            raise ValueError("Health should be an int.")
        if self.health < 0:
            self._health = 0

    @property
    def attack(self):
        return self._attack

    @attack.setter
    def attack(self, val):
        """
        checks if the attack that was set is an integer
        """
        if isinstance(val, int):
            self._attack = val
        else:
            raise ValueError("Attack should be an int.")

    @property
    def defence(self):
        return self._defence

    @defence.setter
    def defence(self, val):
        """
        checks if the defence that was set is an integer
        """
        if isinstance(val, int):
            self._defence = val
        else:
            raise ValueError("Defence should be an int.")

    def char(self):
        """
        returns characters attributes
        """
        return f"""Character\'s attributes are:
name = {self.name}
health = {self.health}
attack = {self.attack}
defence = {self.defence}
"""

    def hit(self, other):
        """
        attacking another player
        decreasing his health
        if health is 0 makes attacker a winner
        """
        other.health = (other.health - self.attack + other.defence)
        if other.health == 0:
            return f'You attacked {other.name}. He lost! You won the game!'
        return f'You attacked {other.name}. His health now is {other.health}.'


u1 = Unit('NoobPlayer101', 100, 15, 10)
u2 = Unit('ProPlayerLol', 200, 30, 50)

print("""Before attack:
""")
print(u1.char(), u2.char())

hitting_u1 = u2.hit(u1)

print(hitting_u1)
print("""After attack:
""")

print(u1.char(), u2.char())
